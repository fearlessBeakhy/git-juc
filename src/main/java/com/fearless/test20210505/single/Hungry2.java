package com.fearless.test20210505.single;

public class Hungry2 {

    private static final Hungry2 hungry;

    static {
        hungry=new Hungry2();
    }

    private Hungry2() {
    }


    public static Hungry2 getInstance(){
        return hungry;
    }
}
