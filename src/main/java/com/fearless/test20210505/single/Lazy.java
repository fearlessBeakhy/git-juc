package com.fearless.test20210505.single;
/*

        懒汉式，单例。

        用的时候再加载

 */

public class Lazy {


    private Lazy() {
        System.out.println("懒汉式创建对象了奥");
    }


    private volatile static Lazy  lazy;


    //DCL双重检查
    public static Lazy getInstance(){

        if (lazy == null) {

            synchronized (Lazy.class){

                if (lazy == null) {
                    lazy = new Lazy();//这个操作不是原子操作，有可能会有重排序问题，故使用volatile。1.分配内存空间
                                                                                            // 2、执行构造方法,初始化对象3、把这个对象指向这个空间
                }

            }

        }

        return lazy;
    }


    public static   void main(String[] args) {
        for (int i = 0; i <20 ; i++) {
            new Thread(()->{
                Lazy.getInstance();
            }).start();
        }
    }
}
