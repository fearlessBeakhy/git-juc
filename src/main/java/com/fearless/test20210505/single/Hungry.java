package com.fearless.test20210505.single;
/*

        饿汉式 单例

    上来就创建对象
 */

public class Hungry {

    private static final Hungry hungry=new Hungry();


    private Hungry() {
        System.out.println("创建对象了 奥");
    }

    public static Hungry newInstanceH(){

        return hungry;
    }

    public static void main(String[] args) {
        for (int i = 0; i <20 ; i++) {
            new Thread(()->{

                Hungry.newInstanceH();
            }).start();
        }
    }

}
