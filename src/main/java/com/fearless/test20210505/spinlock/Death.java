package com.fearless.test20210505.spinlock;

import org.omg.Messaging.SYNC_WITH_TRANSPORT;

import java.util.concurrent.TimeUnit;

public class Death {

   static StringBuffer buffer1=new StringBuffer();
   static StringBuffer buffer2=new StringBuffer();



    public static void main(String[] args) {

        new Thread(()->{
            synchronized (buffer1){
                System.out.println(Thread.currentThread().getName()+"占有了buffer1");

                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                synchronized (buffer2){
                    System.out.println(Thread.currentThread().getName()+"占有了buffer2");
                }

            }

        },"A").start();



        new Thread(()->{
            synchronized (buffer2){
                System.out.println(Thread.currentThread().getName()+"占有了buffer2");
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


                synchronized (buffer1){
                    System.out.println(Thread.currentThread().getName()+"占有了buffer1");

                }

            }







        },"B").start();

    }


}
