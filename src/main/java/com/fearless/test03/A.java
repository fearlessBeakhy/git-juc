package com.fearless.test03;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/*

        使用JUC,完成如下操作
        A执行完调用B，B执行完调用C，C执行完调用A

 */



public class A {
    public static void main(String[] args) {

        new Thread(()->{},"A").start();
        new Thread(()->{},"B").start();
        new Thread(()->{},"C").start();

    }

}

//共享资源类
class Data1{

    private Lock lock=new ReentrantLock();
    Condition condition = lock.newCondition();


    void printA(){
        lock.lock();
        try {

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }

    }

    void printB(){

        lock.lock();
        try {

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }


    }

    void printC(){

        lock.lock();
        try {

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }


    }






}