package com.fearless.test20210503.forkjoin;

import java.util.concurrent.RecursiveTask;

/*

    求和计算

    3000  6000（ForkJoin） 9000（Stream并行流）

 */
public class ForkJoinDemo extends RecursiveTask<Long> {
//使用ForkJoin来计算

    private long start;//起始值
    private long end;//末尾值

    private long temp=10000L;//临界值

    public ForkJoinDemo(long start, long end) {
        this.start = start;
        this.end = end;
    }

    protected Long compute() {
        if((start-end)<temp){
            //因为数据量小，采用常规方式，
            long sum=0L;
            for (Long i = start; i <=end ; i++) {
                sum+=i;
            }
            return sum;
        }else{
            //采用ForkJoin。程序走到这里，说明计算的数据量比较大。
            long middle=(start+end)/2;//获取中间值1、

            //将任务拆分。拆分成两个
            ForkJoinDemo task1 = new ForkJoinDemo(start, middle);
            task1.fork();//将任务压入线程队列
            ForkJoinDemo task2 = new ForkJoinDemo(middle + 1, end);
            task2.fork();


            return  task1.join() + task2.join();

        }
        
        
    }



    public static void main(String[] args) {
        ForkJoinDemo forkJoinDemo = new ForkJoinDemo(1, 99);
        Long aLong = forkJoinDemo.compute();
        System.out.println(aLong);
    }


}
