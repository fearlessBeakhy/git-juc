package com.fearless.test20210504.volatileTest;

import java.util.concurrent.atomic.AtomicInteger;

/*

        验证volatile不保证原子性

 */
public class JMMDemo2 {

/*
     private  static  volatile int  num=0;//验证完毕，volatile无法保证原子性。
*/

     //我们这里使用原子类来保证原子性。不适应锁
    private static  volatile AtomicInteger num=new AtomicInteger();

    public   synchronized  static void add(){
        //num++;
        num.getAndIncrement();
    }


    public static void main(String[] args) {

        for (int i = 0; i <20 ; i++) {

            new Thread(()->{
                for (int j = 0; j <1000 ; j++) {
                    add();
                }
            }).start();
        }


        while (Thread.activeCount()>2){
            Thread.yield();
        }

        System.out.println("num的结果是"+num);//理论应该是2万

    }



}
