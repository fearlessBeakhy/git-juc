package com.fearless.test20210502.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/*

    Executors:线程工具类
    三大方法

    使用线程池来创建线程

 */
public class Demo1 {

    public static void main(String[] args) {
        ExecutorService threadPool = Executors.newSingleThreadExecutor();//单个线程池
        //ExecutorService threadPool = Executors.newFixedThreadPool(3);//创建一个固定大小的线程池
        Executors.newCachedThreadPool();//创建一个带缓存的线程池。可伸缩的


        try {
            for (int i = 0; i <10 ; i++) {
                threadPool.execute(()->{//使用线程池来创建线程
                    System.out.println(Thread.currentThread().getName()+".");
                });

            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            threadPool.shutdown();//关闭线程池
        }


    }

}
