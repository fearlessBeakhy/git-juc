package com.fearless.test20210502.pool;

/*

    用代码模拟线程池银行办理业务的场景

 */


import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.concurrent.*;

public class Demo2 {
//自定义线程池
    public static void main(String[] args) {


        //使用原生的ThreadPoolExecutor来创建线程池。这是阿里开发手册中所提到的
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(
                2,
                5,
                3,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(3),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy()//第一种拒绝策略，直接抛异常
                );

        System.out.println(Runtime.getRuntime().availableProcessors());//获取当前计算机的核数


        List<Integer> list = Arrays.asList(1, 2, 3);

        list.forEach(System.out::println);


    }



}
