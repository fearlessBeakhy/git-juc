package com.fearless.test20210514;


import lombok.AllArgsConstructor;
import lombok.Getter;
/*

枚举的简单使用 2021年5月14日15:19:13
 */


@AllArgsConstructor
public enum CountryEnum {

    ONE(1,"齐"),TWO(2,"楚"),THREE(3,"燕"),
    FOUR(4,"韩"),FIVE(5,"赵"),SIX(6,"魏国");


    @Getter private Integer retCode;

    @Getter private String retMessage;

    public static CountryEnum foreach_countryEnum(int index){
        CountryEnum[] enums = CountryEnum.values();
        for (CountryEnum element : enums) {

            if (index==element.getRetCode()){
                return element;
            }
        }



        return null;
    }

}
