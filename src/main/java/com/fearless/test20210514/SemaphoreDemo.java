package com.fearless.test20210514;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/*
    2021年5月14日15:18:33 李红磊。信号量

 */

public class SemaphoreDemo {
    public static void main(String[] args) {

        Semaphore semaphore = new Semaphore(3);//最大并发数：3。总共3个车位


        for (int i = 1; i <=6 ; i++) {//6辆车
            new Thread(()->{
                try {
                    semaphore.acquire();//停车位-1
                    System.out.println(Thread.currentThread().getName()+"\t抢到车位");
                    TimeUnit.SECONDS.sleep(3);//停3秒钟

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    semaphore.release();
                    System.out.println(Thread.currentThread().getName()+"\t释放车位");
                }

            },String.valueOf(i)).start();
        }


    }


}
