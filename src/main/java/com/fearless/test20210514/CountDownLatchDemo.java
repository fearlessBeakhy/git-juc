package com.fearless.test20210514;


import java.util.concurrent.CountDownLatch;

/*
    减法计数器。2021年5月14日15:18:47 李红磊

 */

public class CountDownLatchDemo {

    public static void main(String[] args) throws InterruptedException {

        CountDownLatch countDownLatch = new CountDownLatch(6);//计数器初始化为6


        for (int i = 1; i <=6 ; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"被灭了");
                countDownLatch.countDown();//计数器--
            },CountryEnum.foreach_countryEnum(i).getRetMessage()).start();
        }



        countDownLatch.await();//计数器为零才会执行下面代码
        System.out.println("***********秦，统一华夏");

    }


}
