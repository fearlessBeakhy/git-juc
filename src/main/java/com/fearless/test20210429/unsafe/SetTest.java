package com.fearless.test20210429.unsafe;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/*

    java.util.ConcurrentModificationException并发修改异常，

    解决HashSet线程不安全问题
    1.使用工具类Collections的synchronizedSet()将HashSet转为线程安全
    2.使用JUC下面的CopyOnWriteArraySet

 */



public class SetTest {

    public static void main(String[] args) {
        HashSet<String> set = new HashSet<>();

        Set<String> set2 = Collections.synchronizedSet(new HashSet<String>());

        for (int i = 0; i <20 ; i++) {

            new Thread(()->{
                set2.add(UUID.randomUUID().toString().substring(0,5));
                System.out.println("线程-->"+Thread.currentThread().getName()+"添加完元素。现在桶中的元素有："+set2);
            },String.valueOf(i)).start();
        }




    }



}
