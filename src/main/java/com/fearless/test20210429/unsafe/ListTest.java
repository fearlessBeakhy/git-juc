package com.fearless.test20210429.unsafe;

import com.fearless.test03.A;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/*
    Integer i=Integer.valueOf(arg) 将arg转为Integer类型的
    String  s=String.valueOf(arg)  将arg转为String类型的


    java.util.ConcurrentModificationException 并发修改异常

 */

public class ListTest {

    public static void main(String[] args) {

    //解决ArrayList线程安全问题的方式
    //解决方案1：使用Vector
    //解决方案2:使用集合工具类，将ArrayList转为线程安全
    //解决方案3；使用JUC下面的CopyOnWriteArrayList



       /* //使用集合工具类，将ArrayList转为线程安全
        List<String> arrayList = Collections.synchronizedList(new ArrayList<>());*/

        List<String> list = new CopyOnWriteArrayList<>();//写入时复制


        for (int i = 0; i <10 ; i++) {

            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,5));
                System.out.println(list);
            },"A").start();

        }

/*
        arrayList.forEach(System.out::println);
*/


    }


}

