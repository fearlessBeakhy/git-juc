package com.fearless.test20210501;

/*
辅助类
    加法计数器。塞克累克掰瑞奥

 */


import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierTest {

    public static void main(String[] args) {
//集齐七颗龙珠召唤神龙

        CyclicBarrier cyclicBarrier = new CyclicBarrier(7,()->{
            System.out.println("召唤神龙成功");
        });


        for (int i = 0; i <7 ; i++) {
            final int temp=i+1;
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"收集了第"+temp+"个龙珠");

                try {
                    cyclicBarrier.await();//等到加法计数器的值到达7，程序才会继续向下走
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            },String.valueOf(i)).start();




        }


    }



}
