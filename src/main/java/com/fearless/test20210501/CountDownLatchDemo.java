package com.fearless.test20210501;


import java.util.concurrent.CountDownLatch;

/*
辅助类
    减法计数器

 */
public class CountDownLatchDemo {


    public static void main(String[] args) throws InterruptedException {

        //倒计时6秒
        CountDownLatch countDownLatch = new CountDownLatch(6);

        for (int i = 0; i <6 ; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"溜了溜了");
                countDownLatch.countDown();//执行一次，计数器减1
                },String.valueOf(i)).start();
        }

        countDownLatch.await();//等到计数器归零了，程序再继续向下执行

        System.out.println("关门了");


    }



}
