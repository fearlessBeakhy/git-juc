package com.fearless.test20210501;


/*
辅助类
    信号量

 */
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class SemaphoreTest {

    public static void main(String[] args) {

        //参数：线程数量。这里模拟停车位，参数表示停车位的数量。也可以看做是现实中的限流
        Semaphore semaphore = new Semaphore(3);//两个方法acquire()获得 release()释放

        for (int i = 0; i <6 ; i++) {
            new Thread(()->{
                try {
                    semaphore.acquire();//获取停车位，占领一个停车位
                    System.out.println(Thread.currentThread().getName()+"抢到车位");
                    TimeUnit.SECONDS.sleep(2);
                    System.out.println(Thread.currentThread().getName()+"离开车位");

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    semaphore.release();//释放车位
                }

            },String.valueOf(i)).start();


        }


    }



}
