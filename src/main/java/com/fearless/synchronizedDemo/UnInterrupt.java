package com.fearless.synchronizedDemo;

/*

    演示synchronized的不可中断

    步骤：1.定义一个Runnable
         2.在Runnable中定义同步代码块
         3.先开启一个线程来执行同步代码块，保证不退出同步代码块
         4.后开启一个线程来执行同步代码块(该线程将会处于阻塞状态)
         5.停止第二个线程

 */



public class UnInterrupt {

    private static Object obj=new Object();


    public static void main(String[] args) {

      /*  //1.定义一个Runnable
       Runnable runnable= ()->{

           //2.在Runnable中定义同步代码块
          synchronized (obj){
              System.out.println(Thread.currentThread().getName()+"进入到同步代码块");
              try {
                  Thread.sleep(1000*10);
              } catch (InterruptedException e) {
                  e.printStackTrace();
              }
          }

        };

       //3.先开启一个线程来执行同步代码块，保证不退出同步代码块
        Thread thread1=new Thread(runnable);
        thread1.start();

        //4.后开启一个线程来执行同步代码块(该线程将会处于阻塞状态)
        Thread thread2 = new Thread(runnable);
        thread2.start();


        //5.停止第二个线程
        System.out.println("中断线程前");
        thread2.interrupt();
        System.out.println("中断线程后");


        System.out.println(thread1.getState());
        System.out.println(thread2.getState());
*/
    }



}

