package com.fearless.test01;

public class MyThread {

    public static void main(String[] args) {
        Ticket ticket=new Ticket();

        new Thread(()->{
            for (int i = 0; i <20 ; i++) {
                ticket.saleTicket();

            }

        },"A").start();


        new Thread(()->{
            for (int i = 0; i <20 ; i++) {
                ticket.saleTicket();

            }
        },"B").start();
    }

}

 class Ticket{
    private Integer ticket=40;

    synchronized void saleTicket(){
        if(ticket>0){
            System.out.println(Thread.currentThread().getName()+"卖票了。卖的是第"+ticket+"张票，还剩余"+(--ticket)+"张票");
        }
    }


}